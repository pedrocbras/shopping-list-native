import React, { useState } from 'react'
import { View, StyleSheet, FlatList, Image, Alert } from 'react-native'
import Header from './components/Header'
import ListItem from './components/ListItem'
import AddItem from './components/AddItem'

const App = () => {
  const [items, setItems] = useState([
    {
      id: `${Math.floor(Math.random() * 1000000)}`,
      label: 'Milk',
    },
    {
      id: `${Math.floor(Math.random() * 1000000)}`,
      label: 'Eggs',
    },
    {
      id: `${Math.floor(Math.random() * 1000000)}`,
      label: 'Bread',
    },
    {
      id: `${Math.floor(Math.random() * 1000000)}`,
      label: 'Juice',
    },
    {
      id: `${Math.floor(Math.random() * 1000000)}`,
      label: 'Cereal',
    },
  ])

  const deleteItem = (id) => {
    setItems((prevItems) => {
      return prevItems.filter((item) => item.id !== id)
    })
  }

  const addItem = (item) => {
    if (!item) {
      Alert.alert('Error', 'Please add an item', { text: 'Ok' })
    } else {
      setItems((prevItems) => {
        return [
          { id: `${Math.floor(Math.random() * 1000000)}`, label: item },
          ...prevItems,
        ]
      })
    }
  }

  return (
    <View style={styles.container}>
      <Image
        source={{
          uri:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTZtIFuelTnfxMfBYykLuNvF-bx7y_htRDkHRKIRZLzI44DsC2t&usqp=CAU',
        }}
        style={styles.background}
      />
      <Header title={'Shopping List'} />
      <AddItem addItem={addItem} />
      <FlatList
        data={items}
        renderItem={({ item }) => (
          <ListItem item={item} deleteItem={deleteItem} />
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
  },
  background: {
    position: 'absolute',
    flex: 1,
    height: '100%',
    width: '100%',
  },
})

export default App
