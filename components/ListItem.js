import React from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'

const ListItem = (props) => {
  const { item, deleteItem } = props
  const { label, id } = item
  return (
    <TouchableOpacity style={styles.listItem}>
      <View style={styles.listItemView}>
        <Text style={styles.listItemText}>{label}</Text>
        <Icon
          name={'remove'}
          size={20}
          color={'firebrick'}
          onPress={() => deleteItem(id)}
        />
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  listItem: {
    margin: 5,
    padding: 15,
    backgroundColor: '#f8f8f8',
    borderRadius: 10,
  },
  listItemView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItemText: {
    fontSize: 16,
  },
})

export default ListItem
