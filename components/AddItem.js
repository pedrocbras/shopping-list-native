import React, { useState } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'

const AddItem = (props) => {
  const [text, setText] = useState('')
  const { addItem } = props

  const onChange = (value) => setText(value)
  return (
    <View>
      <TextInput
        placeholder={'Add item...'}
        style={styles.input}
        onChangeText={onChange}
      />
      <TouchableOpacity style={styles.button} onPress={() => addItem(text)}>
        <Text style={styles.text}>
          <Icon name={'plus'} size={20} />
          Add
        </Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  input: {
    height: 60,
    padding: 8,
    fontSize: 16,
  },
  button: {
    backgroundColor: 'white',
    padding: 9,
    margin: 5,
    width: 100,
    borderRadius: 5,
  },
  text: {
    color: 'red',
    fontSize: 20,
    textAlign: 'center',
  },
})

export default AddItem
